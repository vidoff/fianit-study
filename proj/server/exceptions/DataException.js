class DataException {
    constructor(message) {
        this.message = message;
        this.name = "GET_DATA EXCEPTION";
    }
}
module.exports = DataException;
