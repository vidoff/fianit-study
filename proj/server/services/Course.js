const debug = require("debug")("app:services:course");
const path = require("path");
const conf = require("../data/conf.json");
var Transport = require("../transport/wsdl");
var DataException = require("../exceptions/DataException");

const data = {
    GET_COURSES: "../data/courses.json",
    GET_COURSE_DETAIL: "../data/courseDetail.json"
};

class Course {
    constructor(props = {}) {
        this.props = props;
        this.options = Object.assign({}, conf.courses, data);
        // this.type = "GET_COURSES";
    }
    __getStatusName(status_id) {
        for (let i = 0; i < this.options.statuses.length; i++) {
            if (Number(status_id) === this.options.statuses[i].id)
                return this.options.statuses[i].name;
        }
        return "undef";
    }
    __prepare(data) {
        debug('%O', this.props);
        debug('data: %O', data);
        //Добавляем названия к статусам курсов по их id
        if (this.props.type == "GET_COURSES") {
            try {
                data.items.map(el => {
                    el.status_name = this.__getStatusName(el.status);
                    return el;
                });
            } catch (e) {
                throw new DataException("data.items is not array");
            }
        }
        return data;
    }

    getMethod() {
        switch (this.props.type) {
            case "GET_COURSE_DETAIL":
                return this.getCourseDetail;
            default:
                return this.getCourses;
        }
    }

    getCourseDetail() {
        const wsdl = new Transport();
        return Promise.resolve(wsdl.send())
            .then(result => {
                debug(`result: ${result}`);
                return this.__prepare(result);
            })
            .catch(error => {
                // throw error;
                debug(`ERROR COURSE_DETAIL: ${error.message}`);
                return this.__prepare({...this.__getTestData(), id: this.props.course.id});
            });
    }

    getCourses() {
        const wsdl = new Transport();

        return Promise.resolve(wsdl.send())
            .then(result => {
                debug(`result: ${result}`);
                return this.__prepare(result);
            })
            .catch(error => {
                debug(`ERROR: ${error.message}`);
                return this.__prepare(this.__getTestData());
            });
    }
    __getTestData() {
        let data = require(this.options[this.props.type]);
        return data;
    }
    render() {
        return this.data;
    }
}
module.exports = Course;
