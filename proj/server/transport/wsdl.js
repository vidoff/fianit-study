const debug = require("debug")("app:transport:wsdl");
// const _ = require('lodash');
const soap = require("soap");

const settings = {
    // url: "http://wstest.dpd.ru/services/calculator2?wsdl",
    url: "",
    method: "getServiceCostByParcels2"
};

class WSDL {
    constructor(options) {
        this.options = { ...settings, options };
    }
    send(params) {
        return soap
            .createClientAsync(this.options.url)
            .then(client => {
                const query = {};
                return client[this.options.method + "Async"](query);
            })
            .then(result => {
                debug(`result: ${result}`);
                return result;
            });
    }
}
module.exports = WSDL;
