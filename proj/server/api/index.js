const debug = require("debug")("app:api:index");
var express = require("express");
var router = express.Router();

const Course = require("../services/Course");

router.post("/courses", function(req, res, next) {
    let course = new Course(req.body);
    let method = course.getMethod(req.body.type);
    method
        .call(course)
        .then(data => {
            // debug(`data %O:`, data);
            res.send(data);
        })
        .catch(error => {
            debug(`error: ${error.message}`);
            res.status(400).send(error.message);
        });

    // course
    //     .getCourses()
    //     .then(data => {
    //         res.send(data);
    //     })
    //     .catch(error => {
    //         res.status(400).send(error);
    //     });
});

module.exports = router;
