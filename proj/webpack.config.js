const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require("path");
var isDev = false;

var config = {
    entry: "./webapp/index.js",
    output: {
        path: path.resolve(__dirname, "static/js"),
        filename: "main.js"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        plugins: ["babel-plugin-redux-saga"]
                    }
                }
            },
            {
                test: /\.(svg|png|woff|woff2|eot|ttf)$/,
                // include: [path.join(__dirname, "/webapp/static/css/fonts")],
                loader: "url-loader",
                // loader: "file-loader",
                options: {
                    limit: 100000,
                    // emitFile: false,
                    name: "../assets/[hash].[ext]"
                }
            },
            {
                test: /\.css$/,
                exclude: [
                    /node_modules/,
                    path.resolve(__dirname, "static/css/postcss/raw")
                ],
                use: [
                    "style-loader",
                    // MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader",
                        options: {
                            importLoaders: 1,
                            modules: true,
                            camelCase: true,
                            localIdentName:
                                "[path]___[name]__[local]___[hash:base64:5]"
                        }
                    },
                    "postcss-loader"
                ]
            },
            {
                test: /\.css$/,
                include: [
                    /node_modules/,
                    path.join(__dirname, "static/css/postcss/raw/")
                ],
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "postcss-loader"
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "../css/main.css",
            // chunkFilename: "[name].[contenthash].css"
        })
    ]
};

module.exports = (env, argv) => {
    if (argv.mode === "development") {
        config.devtool = "source-map";
        isDev = true;
    }
    return config;
};
