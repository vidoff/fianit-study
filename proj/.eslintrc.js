module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "node": true,
    },
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended",
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parser":"babel-eslint",
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true,
            // "modules": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    // "ecmaFeatures": {
    //     "jsx": true,
    //     "modules": true,
    // },
    "plugins": [
        "react"
    ],
    "rules": {
        // "strict": [2, "never"],
        // "react/jsx-uses-react": 2,
        // "react/jsx-uses-vars": 2,
        // "react/react-in-jsx-scope": 2,
        "no-console": "off"
    },
    "settings": {
        "react": {
            "version": "detect"
        }
    }
};
