// export const registerUserService = request => {
//     const REGISTER_API_ENDPOINT = "http://localhost:3000/api/v1/register";

//     const parameters = {
//         method: "POST",
//         headers: {
//             "Content-Type": "application/json"
//         },
//         body: JSON.stringify(request.user)
//     };

//     return fetch(REGISTER_API_ENDPOINT, parameters)
//         .then(response => {
//             return response.json();
//         })
//         .then(json => {
//             return json;
//         });
// };

export const loginUserService = request => {
    const LOGIN_API_ENDPOINT = "http://localhost:3000/api/v1/login";

    const parameters = {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(request.user)
    };
    const user = request.user;

    // return fetch(LOGIN_API_ENDPOINT, parameters)
    //     .then(response => {
    //         return response.json();
    //     })
    //     .then(json => {
    //         return json;
    //     })
    return new Promise((resolve, reject) => {
        const response = {
            user: {
                is_auth: true,
            }
        }
        if(response.user.is_auth){
            setTimeout(function() {
                resolve(Object.assign(user, response.user));
            }, 2000);
        } else {
            reject('not equal');
        }
    });
};
