export const getCoursesService = request => {
    const headers = new Headers({
        "Content-Type": "application/json"
    });
    const COURSES_ENDPOINT = "/api/courses";

    const parameters = {
        method: "POST",
        headers: headers,
        body: JSON.stringify(request)
    };

    return fetch(COURSES_ENDPOINT, parameters)
        .then(response => {
            return response.json();
        })
        .then(json => {
            return json;
        });
};

export const getCourseDetailService = request => {
    const headers = new Headers({
        "Content-Type": "application/json"
    });
    const COURSES_ENDPOINT = "/api/courses";

    const parameters = {
        method: "POST",
        headers: headers,
        body: JSON.stringify(request),
    };

    return fetch(COURSES_ENDPOINT, parameters)
        .then(response => {
            return response.json();
        })
        .then(json => {
            return json;
        });
};
