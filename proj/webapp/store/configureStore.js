import "regenerator-runtime/runtime";
import { applyMiddleware, createStore } from "redux";
import logger from "redux-logger";
import { composeWithDevTools } from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";
import rootReducer from "../reducers/rootReducer";
import rootSaga from "../sagas/rootSaga";

export default function configureStore(preloadedState) {
    const sagaMiddleware = createSagaMiddleware();
    const middlewares = [logger, sagaMiddleware];
    const middlewareEnhancer = applyMiddleware(...middlewares);

    // const enhancers = [middlewareEnhancer, monitorReducersEnhancer];
    const enhancers = [middlewareEnhancer];
    const composedEnhancers = composeWithDevTools(...enhancers);

    const store = createStore(rootReducer, preloadedState, composedEnhancers);

    // if (process.env.NODE_ENV !== "production" && module.hot) {
    //     module.hot.accept("./reducers", () =>
    //         store.replaceReducer(rootReducer)
    //     );
    // }

    return { ...store, runSaga: sagaMiddleware.run(rootSaga) };
}
