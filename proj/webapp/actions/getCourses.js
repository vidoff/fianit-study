import * as types from "./index";
export const getCoursesAction = courses => {
    return {
        type: types.GET_COURSES,
        courses
    };
};

export const getCourseDetailAction = data => {
    return {
        type: types.GET_COURSE_DETAIL,
        ...data
    };
};
