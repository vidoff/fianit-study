import { fork } from "redux-saga/effects";
// import watchUserAuthentication from "./watchers";
import * as watcher from "./watchers";
// import { watchUserAuthentication, watchRequestGetCourses } from "./watchers";

export default function* rootSaga() {
    yield fork(watcher.watchUserAuthentication);
    yield fork(watcher.watchRequestGetCourses);
    yield fork(watcher.watchRequestGetCourseDetail);
}
