import { put, call } from "redux-saga/effects";
import { loginUserService } from "../services/authService";
import {
    getCoursesService,
    getCourseDetailService
} from "../services/courseService";
import * as types from "../actions";

export function* loginSaga(payload) {
    try {
        const response = yield call(loginUserService, payload);
        // yield [put({ type: types.LOGIN_USER_SUCCESS, response })];
        yield put({ type: types.LOGIN_USER_SUCCESS, response });
    } catch (error) {
        yield put({ type: types.LOGIN_USER_ERROR, error });
    }
}

export function* getCoursesSaga(payload) {
    const request = { ...payload, user: { id: 1 } };
    try {
        const response = yield call(getCoursesService, request);
        yield put({ type: types.GET_COURSES_SUCCESS, response });
    } catch (error) {
        yield put({ type: types.GET_COURSES_ERROR, error });
    }
}

export function* getCourseDetailSaga(payload) {
    try {
        const response = yield call(getCourseDetailService, payload);
        yield put({ type: types.GET_COURSE_DETAIL_SUCCESS, response });
    } catch (error) {
        yield put({ type: types.GET_COURSE_DETAIL_ERROR, error });
    }
}
