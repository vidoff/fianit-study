import { takeLatest } from "redux-saga/effects";
import * as actions from "./actions";
import * as types from "../actions";

// export default function* watchUserAuthentication() {
//     yield takeLatest(types.LOGIN_USER, loginSaga);
// }
export function* watchUserAuthentication() {
    yield takeLatest(types.LOGIN_USER, actions.loginSaga);
}

export function* watchRequestGetCourses() {
    yield takeLatest(types.GET_COURSES, actions.getCoursesSaga);
}

export function* watchRequestGetCourseDetail() {
    yield takeLatest(types.GET_COURSE_DETAIL, actions.getCourseDetailSaga);
}
