import { combineReducers } from "redux";
import login from "./loginReducer";
import { courses, courseDetail } from "./coursesReducer";

const rootReducer = combineReducers({
    login,
    courses,
    courseDetail
});
export default rootReducer;
