import * as types from "../actions";

export function courses(state = {}, action) {
    const response = action.response;
    switch (action.type) {
        case types.GET_COURSES_SUCCESS:
            return { ...state, ...response, is_loaded: true };
        case types.GET_COURSES_ERROR:
            return { ...state, ...response };
        default:
            return state;
    }
}

export function courseDetail(state = {}, action) {
    const response = action.response;
    switch (action.type) {
        case types.GET_COURSE_DETAIL_SUCCESS:
            return { ...state, ...response, is_loaded: true };
        case types.GET_COURSE_DETAIL_ERROR:
            return { ...state, ...response };
        default:
            return state;
    }
}
