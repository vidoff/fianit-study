import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import key from "weak-key";


class LectureList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>lectures</div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ...state
    };
};

LectureList.propTypes = {
    courses: PropTypes.object.isRequired,
};

export default connect(mapStateToProps)(LectureList);
