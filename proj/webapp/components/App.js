import React, { Component } from "react";
import { Route, Switch, BrowserRouter } from "react-router-dom";
// import { connect } from "react-redux";
import Main from "./Main";
import "styles/raw/style.css";
import LoginPage from "./LoginPage";

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path="/login" component={LoginPage} />
                    <Route path="/" component={Main} />
                    <Route render={() => <div>Not Found</div>} status={404} />
                </Switch>
            </BrowserRouter>
        );
    }
}

// export default connect(mapStateToProps, mapDispatchToProps)(App);
export default App;
