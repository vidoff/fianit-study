import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const Button = styled.div`
    width: 23px;
    height: 23px;
    border: 0;
    background: none;
    display: flex;
    align-items: center;
    padding: 0;
    cursor: pointer;
    margin-right: 24px;
    &:last-child {
        margin-right: 0;
    }
    &:focus {
        outline: none;
    }
    &.is-active {
        opacity: 0.5;
    }
`;
const Image = styled.div`
        width: 100%;
        height: 100%;
        background: url('${props => props.src}') center center no-repeat;
`;

const ButtonListView = props => {
    return (
        <Button>
            <Image
                src={props.image}
                onClick={props.onClick}
                data-view={props.dataView}
            />
        </Button>
    );
};

ButtonListView.propTypes = {
    dataView: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    image: PropTypes.string.isRequired
};

export default ButtonListView;
