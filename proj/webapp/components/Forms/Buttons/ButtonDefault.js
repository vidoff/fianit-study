import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const Styled = `
    font-family: "Proxima Nova Bold", serif;
    border: 1px solid #1561d4;
    border-radius: 60px;
    background: none;
    background-color: transparent;
    color: #1561d4;
    text-transform: uppercase;
    font-size: 12px;
    cursor: pointer;
    line-height: 16px;
    transition: background-color 150ms linear;
    &:focus {
        outline: none;
    }
    &:hover {
        color: #fff;
        background-color: #1561d4;
    }
`;

const StyledButton = styled.button`
    ${Styled}
    padding: 0;
`;

const ButtonText = styled.div`
    padding: 16px 25px 13px;
`;

const StyledLink = styled(Link)`
    ${Styled}
    text-decoration: none;
    display: inline-block;
    &:hover {
        text-decoration: none;
    }
`;

const ButtonDefault = props => {
    const content = props.children ? props.children : null;
    if(props.to){
        return (
            <StyledLink {...props}>
                <ButtonText>{content}</ButtonText>
            </StyledLink>
        );
    }
    return (
        <StyledButton {...props}>
            <ButtonText>{content}</ButtonText>
        </StyledButton>
    );
};

ButtonDefault.propTypes = {
    to: PropTypes.string
};

export default ButtonDefault;
