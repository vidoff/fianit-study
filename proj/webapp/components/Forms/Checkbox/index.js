import React from "react";
import styled from "styled-components";

const HiddenCheckbox = styled.input.attrs({ type: "checkbox" })`
    border: 0;
    clip: rect(0 0 0 0);
    clippath: inset(50%);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    white-space: nowrap;
    width: 1px;
`;

const Icon = styled.svg`
    fill: none;
    stroke: white;
    stroke-width: 2px;
`;
const CheckedStyles = `
`;
const UncheckedCheckedStyles = `
    background: transparent;
    &:after {
        position: absolute;
        content: "";
        width: 13px;
        height: 13px;
        border: 2px solid #2f80ed;
        border-radius: 3px;
        top: 0;
        left: 0;
    }
`;
const StyledCheckbox = styled.div`
    display: inline-block;
    margin-top: 2px;
    position: relative;
    width: 17px;
    height: 17px;
    box-sizing: border-box;
    border-radius: 3px;
    transition: all 150ms;
    background: ${props => (props.fill ? props.fill : "#219653")};
    ${props => (props.checked ? CheckedStyles : UncheckedCheckedStyles)}
    ${Icon} {
        visibility: ${props => (props.checked ? "visible" : "hidden")};
    }
`;
// ${HiddenCheckbox}:focus + & {
//     box-shadow: 0 0 0 3px pink;
// }

const CheckboxContainer = styled.div`
    display: inline-block;
    vertical-align: middle;
`;

const Checkbox = ({ className, checked, ...props }) => (
    <CheckboxContainer className={className}>
        <HiddenCheckbox checked={checked} {...props} />
        <StyledCheckbox checked={checked} fill={props.fill ? props.fill : ""}>
            <Icon viewBox="0 0 24 24">
                <polyline points="20 6 9 17 4 12" />
            </Icon>
        </StyledCheckbox>
    </CheckboxContainer>
);

export default Checkbox;
