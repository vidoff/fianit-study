import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import getFont from "components/Forms/Fonts";

export const TableHeader = styled.div`
    display: grid;
    color: #b2b2b2;
    font-size: 14px;
    text-transform: uppercase;
    ${getFont("semi-bold")}
    & > div {
        grid-row: 1;
    }
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
`;
export const TableContent = styled.div`
    display: grid;
    grid-row-gap: 56px;
`;
export const Row = styled.div`
    display: grid;
    align-items: center;
    &:first-child {
        margin-top: 28px;
    }
    &:last-child {
        margin-bottom: 28px;
    }
`;
const Table = styled.div`
    display: grid;
    ${TableHeader}, ${Row} {
        grid-template-columns: minmax(10px, 2fr) repeat(
                ${props => (props.col ? props.col - 1 : 1)},
                1fr
            );
        grid-column-gap: 30px;
    }
    ${TableContent} {
        ${props => (props.fontWeight ? getFont(props.fontWeight) : null)}
    }
`;

const TableGrid = props => {
    return <Table {...props} />;
};

export default TableGrid;
