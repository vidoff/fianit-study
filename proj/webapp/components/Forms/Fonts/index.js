const PropximaRegular = `
    font-family: "Proxima Nova Rg", serif;
`;

const PropximaSB = `
    font-family: "Proxima Nova SB", serif;
`;

const PropximaBold = `
    font-family: "Proxima Nova Bold", serif;
`;

export default function getFont(fontWeight) {
    switch (fontWeight) {
        case 'normal':
            return PropximaRegular;
        case 'semi-bold':
            return PropximaSB;
        case 'bold':
            return PropximaBold;
        default:
            return PropximaRegular;
    }
}
