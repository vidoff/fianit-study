import React, { Component } from "react";
import styled from "styled-components";
import Checkbox from "components/Forms/Checkbox";
const SelectBoxContainer = styled.div`
    background: #fff;
    box-shadow: 0px 0px 16px rgba(51, 51, 51, 0.24);
    border-radius: 8px;
    padding: 0 12px;
    min-width: 214px;
    position: absolute;
    right: 0;
    top: 0;
`;
const HiddenSelect = styled.div`
    margin-bottom: 14px;
`;
const Dropdown = styled.div`
    display: flex;
    flex-direction: column;
`;

const SelectCaption = styled.div`
    font-size: 12px;
    padding: 12px 0;
`;

class SelectBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showHidden: false,
            value: null
        };
    }
    handleShowHidden = event => {
        event.preventDefault();
        this.setState({ showHidden: !this.state.showHidden });
    };
    handleOnChageStatus = event => {
        this.setState({ value: event.target.value });
    };
    render() {
        return (
            <SelectBoxContainer className={this.props.className}>
                <SelectCaption onClick={this.handleShowHidden}>
                    {this.props.caption}
                </SelectCaption>
                {this.state.showHidden ? (
                    <HiddenSelect>
                        <Dropdown>
                            {this.props.children ? this.props.children : null}
                        </Dropdown>
                    </HiddenSelect>
                ) : null}
            </SelectBoxContainer>
        );
    }
}

export default SelectBox;
