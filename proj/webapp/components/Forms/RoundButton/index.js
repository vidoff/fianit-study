import React from "react";
import styled from "styled-components";

const Arrow = styled.svg`
    fill: none;
    stroke: #1561d4;
    stroke-width: 1px;
    stroke-linecap: round;
    stroke-linejoin: round;
`;
const Button = styled.button`
    border: 0;
    background: #fff;
    border-radius: 50%;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 0;
    cursor: pointer;
    &:focus {
        outline: none;
    }
    &:hover {
        background: #1561d4;
        ${Arrow} {
            stroke: #fff;
        }
    }
    &:disabled {
        opacity: 0.5;
        cursor: auto;
        &:hover {
            background: #fff;
            ${Arrow} {
                stroke: #1561d4;
            }
        }
    }
`;
const RoundButton = ({ className, pos, ...props }) => (
    <Button {...props}>
        <Arrow viewBox="0 0 24 24" width="24" height="24">
            {pos == "left" ? (
                <polyline points="15 18 9 12 15 6" />
            ) : (
                <polyline points="9 18 15 12 9 6" />
            )}
        </Arrow>
    </Button>
);

export default RoundButton;
