import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
// import { startAction } from "../actions/startAction";
// import { stopAction } from "../actions/stopAction";

class DataProvider extends Component {
    static propTypes = {
        endpoint: PropTypes.string.isRequired,
        render: PropTypes.func.isRequired,
        rotating: PropTypes.bool.isRequired
    };

    state = {
        data: [],
        loaded: false,
        placeholder: "Loading..."
    };

    componentDidMount() {
        fetch(this.props.endpoint)
            .then(response => {
                if (response.status !== 200) {
                    return this.setState({
                        placeholder: "Something went wrong"
                    });
                }
                return response.json();
            })
            .then(data => this.setState({ data: data, loaded: true }));
    }

    render() {
        const { data, loaded, placeholder } = this.state;
        // return loaded ? this.props.render(data) : <p>{placeholder}</p>;
        // console.log(this.props);
        return loaded ? (
            <div>
                <h1>DataProvider (rotating: {this.props.rotating ? "yes" : "no"})</h1>
                {this.props.render(data,this.props.rotating)}
            </div>
        ) : (
            <p>{placeholder}</p>
        );
    }
}
// const mapStateToProps = function(state) {
//     return {
//         rotate: state.rotate
//     };
// };
const mapStateToProps = state => ({
    ...state
});
// const mapDispatchToProps = dispatch => ({
//     startAction: () => dispatch(startAction),
//     stopAction: () => dispatch(stopAction)
// });

export default connect(
    mapStateToProps,
    // mapDispatchToProps
)(DataProvider);
