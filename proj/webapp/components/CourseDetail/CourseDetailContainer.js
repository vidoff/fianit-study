import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Header from "components/Header";
import styles from "styles/components/CourseDetail.css";
import ButtonDefault from "components/Forms/Buttons/ButtonDefault";
import LectureList from "components/Lectures/LectureList";
import { getCourseDetailAction } from "../../actions/getCourses";

const options = {
    title: "test",
    header: {
        style: "white"
    }
};

class CourseDetailContainer extends Component {
    constructor(props) {
        super(props);
        this.opts = {
            ...options
        };
    }
    componentDidMount() {
        const data = { ...this.props.match.params };
        this.props.dispatch(getCourseDetailAction(data));
    }
    componentWillUnmount() {
        // Remember state for the next mount
        // localStorage.setItem("appState", JSON.stringify(this.state));
    }

    render() {
        return (
            <div className="layout stretch">
                <Header title={this.opts.title} opts={this.opts.header} />
                <section className="wrapper">
                    <main className="content">
                        <div className="container">
                            <section
                                styleName="styles.descr"
                                className="text containerSection"
                            >
                                <h2
                                    styleName="styles.descrTitle"
                                    className="title"
                                >
                                    Описание курса
                                </h2>
                                <article
                                    styleName="styles.descrContent"
                                    className="text"
                                >
                                    <div styleName="styles.descrText">
                                        descr text
                                    </div>
                                </article>
                                <div styleName="styles.descrAbout">
                                    <div styleName="styles.table">
                                        <div styleName="styles.caption">
                                            Преподаватель
                                        </div>
                                        <div styleName="styles.value">
                                            Поварова Галина Васильевна
                                        </div>
                                        <div styleName="styles.caption">
                                            Продолжительность
                                        </div>
                                        <div styleName="styles.value">
                                            5 дней
                                        </div>
                                    </div>
                                    <ButtonDefault
                                        to="/library"
                                        styleName="styles.buttonLibrary"
                                    >
                                        Перейти в библиотеку курса
                                    </ButtonDefault>
                                </div>
                            </section>
                        </div>
                        <LectureList />
                    </main>
                    <footer>footer</footer>
                </section>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return { ...state };
};

CourseDetailContainer.propTypes = {
    match: PropTypes.object.isRequired
};

export default connect(mapStateToProps)(CourseDetailContainer);
