import React, { Component } from "react";
// import ReactDOM from "react-dom";
// import Logo from "../Logo.js";
import { Route, Link, BrowserRouter as Router } from "react-router-dom";
import styles from "styles/components/SideBar.css";

class SideBar extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <aside styleName="sidebar">
                <div styleName="styles.top">
                    <header>
                        <div styleName="styles.logoWrapper">
                            <Link to="/" styleName="styles.logo" />
                        </div>
                        <div styleName="styles.userProfile">
                            <div styleName="styles.name">
                                Анастасия Владимировна
                            </div>
                            <div styleName="styles.email">
                                <a href="mailto:s.ferguseon@gmail.com">s.ferguseon@gmail.com</a>
                            </div>
                        </div>
                    </header>
                    <nav styleName="styles.nav">
                        <ul>
                            <li styleName="styles.calendar"><Link to="/">Календарь</Link></li>
                            <li styleName="styles.library"><Link to="/library">Библиотека</Link></li>
                            <li styleName="styles.courses"><Link to="/courses">Мои курсы</Link></li>
                            <li styleName="styles.calendar"><Link to="/exam">Зачетная книжка</Link></li>
                        </ul>
                    </nav>
                </div>
                <footer styleName="styles.bottom">
                    <div styleName="styles.helpLink">Нужна помощь?</div>
                    <div styleName="styles.socialButtons">
                        <div styleName="styles.button styles.vk" />
                        <div styleName="styles.button styles.telegram" />
                        <div styleName="styles.button styles.fb" />
                        <div styleName="styles.button styles.viber" />
                    </div>
                </footer>
            </aside>
        );
    }
}
export default SideBar;
