import React, { Component } from "react";
import Checkbox from "components/Forms/Checkbox";
import RoundButton from "components/Forms/RoundButton";
import styles from "styles/components/CalendarFilter.css";
import filter from "styles/common/filter.css";

const months = [
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь"
];

class CalendarFilter extends Component {
    onHandleCheckboxChange = event => {
        this.setState({ checked: event.target.checked });
    };

    render() {
        const handlers = { ...this.props.handlers };
        let data = this.props.data;
        return (
            <div styleName="filter.filter">
                <div styleName="filter.filterGroup filter.checkboxes">
                    <label>
                        <Checkbox
                            checked={data.myCourses}
                            onChange={handlers.handleChangeMyCourses}
                        />
                        <span>Мои курсы</span>
                    </label>
                    <label>
                        <Checkbox
                            checked={data.avCourses}
                            onChange={handlers.handleChangeAvCourses}
                        />
                        <span>Доступные курсы</span>
                    </label>
                </div>
                <div styleName="filter.filterGroup styles.range">
                    <a href="#" onClick={handlers.handleShowByMonth}>
                        Месяц
                    </a>
                    <a href="#" onClick={handlers.handleShowByWeek}>
                        Неделя
                    </a>
                </div>
                <div styleName="filter.filterGroup styles.months">
                    <RoundButton
                        pos="left"
                        onClick={handlers.handlerPrev}
                        disabled={data.showBy.week}
                    />
                    <div styleName="styles.monthCaption">
                        {months[data.date.getMonth()]}
                    </div>
                    <RoundButton
                        pos="right"
                        onClick={handlers.handlerNext}
                        disabled={data.showBy.week}
                    />
                </div>
            </div>
        );
    }
}
export default CalendarFilter;
