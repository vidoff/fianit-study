import React, { Component } from "react";
import PropTypes from "prop-types";
// import Header from "../Header";
// import styles from "styles/components/Calendar.css";
import styles from "styles/components/Table.css";
// import key from "weak-key";
const dayOfWeek = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"];

class Table extends Component {
    constructor(props) {
        super(props);
        const date = new Date();
        this.curDay = date.getDate();
        this.curMonth = date.getMonth();
        this.curYear = date.getFullYear();
    }
    toLocale(day) {
        if (day === 0) {
            return 6;
        }
        return day - 1;
    }
    daysInMonth(year, month) {
        return new Date(year, month + 1, 0).getDate();
    }
    getMonth(date) {
        let days = [];
        let nextMonthCounter = 1;
        const month = date.getMonth();
        const year = date.getFullYear();
        const prevMonthDays = this.daysInMonth(year, month - 1);
        const curMonthDays = this.daysInMonth(year, month);

        // to locale, first monday
        const dayOfWeek = this.toLocale(new Date(year, month, 1).getDay());

        for (let i = 1, j = 0; i <= 42; i++, j++) {
            if (i <= dayOfWeek) {
                days[j] = { day: prevMonthDays - dayOfWeek + i };
            } else if (i <= curMonthDays + dayOfWeek) {
                days[j] = { day: i - dayOfWeek, curMonth: true };
            } else {
                days[j] = { day: nextMonthCounter++ };
            }
        }
        return days;
    }
    render() {
        let calendar = this.getMonth(this.props.date);
        const year = this.props.date.getFullYear();
        const month = this.props.date.getMonth();
        let row = 1;
        return (
            <div styleName="styles.table">
                {calendar.map((el, i) => {
                    let className = "styles.day";
                    if (row == 8) {
                        row = 1;
                    }
                    if (row%6===0 || row%7===0) {
                        className += " styles.weekend";
                    }
                    if (el.curMonth) {
                        className += " styles.curMonth";
                        if (
                            month == this.curMonth &&
                            year == this.curYear &&
                            el.day == this.curDay
                        )
                            className += " styles.curDay";
                    }
                    row++;
                    return (
                        <div key={i} styleName={className}>
                            {i < 7 ? (
                                <div styleName="styles.dayOfWeek">
                                    {dayOfWeek[i]}
                                </div>
                            ) : (
                                ""
                            )}
                            <div styleName="styles.number"><span>{el.day}</span></div>
                        </div>
                    );
                })}
            </div>
        );
    }
}

Table.propTypes = {
    date: PropTypes.object.isRequired
};
export default Table;
