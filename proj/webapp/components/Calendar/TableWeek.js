import React, { Component } from "react";
import PropTypes from "prop-types";
// import styles from "styles/components/Table.css";
// import key from "weak-key";
const dayOfWeek = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"];

class TableWeek extends Component {
    constructor(props) {
        super(props);
        const date = new Date();
    }
    toLocale(day) {
        if (day === 0) {
            return 6;
        }
        return day - 1;
    }
    daysInMonth(year, month) {
        return new Date(year, month + 1, 0).getDate();
    }
    getMonth(date) {
        let days = [];
        let nextMonthCounter = 1;
        const month = date.getMonth();
        const year = date.getFullYear();
        const prevMonthDays = this.daysInMonth(year, month - 1);
        const curMonthDays = this.daysInMonth(year, month);

        // to locale, first monday
        const dayOfWeek = this.toLocale(new Date(year, month, 1).getDay());

        for (let i = 1, j = 0; i <= 42; i++, j++) {
            if (i <= dayOfWeek) {
                days[j] = { day: prevMonthDays - dayOfWeek + i };
            } else if (i <= curMonthDays + dayOfWeek) {
                days[j] = { day: i - dayOfWeek, curMonth: true };
            } else {
                days[j] = { day: nextMonthCounter++ };
            }
        }
        return days;
    }
    render() {
        return (<div>table week</div>)
    }
}
// Table.propTypes = {
//     date: PropTypes.object.isRequired
// };
export default TableWeek;
