import React, { Component } from "react";
import { connect } from "react-redux";
import { getCoursesAction } from "../../actions/getCourses";
import Header from "../Header";
import CalendarFilter from "./CalendarFilter";
import Table from "./Table";
import TableWeek from "./TableWeek";
import styles from "styles/components/Calendar.css";

class Calendar extends Component {
    constructor(props) {
        super(props);
        this.title = "Календарь курсов";
        this.state = {
            date: new Date(),
            myCourses: false,
            avCourses: false,
            showBy: {
                month: true,
                week: false
            }
        };
    }
    handlePrevMonth = event => {
        event.preventDefault();
        const date = this.state.date;
        this.setState({
            date: new Date(date.getFullYear(), date.getMonth() - 1, 1)
        });
    };
    handleNextMonth = event => {
        event.preventDefault();
        const date = this.state.date;
        this.setState({
            date: new Date(date.getFullYear(), date.getMonth() + 1)
        });
    };
    handleChangeMyCourses = event => {
        this.setState({ myCourses: event.target.checked });
    };
    handleChangeAvCourses = event => {
        this.setState({ avCourses: event.target.checked });
    };
    handleShowByMonth = event => {
        event.preventDefault();
        this.setState({ showBy: { month: true, week: false } });
    };
    handleShowByWeek = event => {
        event.preventDefault();
        this.setState({ showBy: { month: false, week: true } });
    };
    componentDidMount() {
        const data = {};
        if (!this.props.courses.is_loaded)
            this.props.dispatch(getCoursesAction(data));
    }

    render() {
        const handlers = {
            handlerNext: this.handleNextMonth,
            handlerPrev: this.handlePrevMonth,
            handleChangeMyCourses: this.handleChangeMyCourses,
            handleChangeAvCourses: this.handleChangeAvCourses,
            handleShowByMonth: this.handleShowByMonth,
            handleShowByWeek: this.handleShowByWeek
        };
        return (
            <div>
                <Header title={this.title}>
                    <CalendarFilter handlers={handlers} data={this.state} />
                </Header>

                <div className="contentBlock text" styleName="styles.container">
                    {this.state.showBy.month ? (
                        <Table date={this.state.date} />
                    ) : (
                        <TableWeek date={this.state.date} />
                    )}
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return { ...state };
};
export default connect(mapStateToProps)(Calendar);
