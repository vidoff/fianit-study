import React, { Component } from "react";
import SideBar from "./SideBar";
import Calendar from "./Calendar";
// import Courses from "./Courses";
import { Route, Redirect, Switch } from "react-router-dom";
import { connect } from "react-redux";
import styles from "styles/components/Main.css";
import Courses from "./Courses/CoursesContainer";
import Library from "./Library";
import CourseDetailContainer from "components/CourseDetail/CourseDetailContainer";

class Main extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        //TODO не забыть включить редирект авторизации
        // if (!this.props.login.is_auth) {
        //     return <Redirect to="/login" />;
        // }
        return (
            <div className="dashboard">
                <SideBar />
                <Route
                    render={({ location }) => (
                        <section styleName="styles.layout">
                            <Switch location={location}>
                                <Route exact path="/" component={Calendar} />
                                <Route exact path="/courses" component={Courses} />
                                <Route path="/courses/:id" component={CourseDetailContainer} />
                                <Route path="/library" component={Library} />
                                <Route
                                    render={() => <div>Not Found</div>}
                                    status={404}
                                />
                            </Switch>
                        </section>
                    )}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ...state
    };
};

export default connect(mapStateToProps)(Main);
