import React, { Component } from "react";
import { Redirect } from "react-router-dom";
// import styles from "styles/components/Layout.css";
import { loginUserAction } from "../../actions/auth";
// import startAction from
import { connect } from "react-redux";
import styles from "styles/components/LoginPage.css";
import form from "styles/common/form.css";
import logo from "styles/components/SideBar.css";

class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            is_loading: false
        };
    }
    onHandleLogin = event => {
        event.preventDefault();
        const data = {
            username: "admin",
            password: "123"
        };
        this.setState({ is_loading: !this.state.is_loading });
        this.props.dispatch(loginUserAction(data));
    };
    render() {
        if (this.props.login && this.props.login.is_auth) {
            return <Redirect to="/" />;
        }

        return (
            <div styleName="styles.login">
                <div styleName="logo.logo styles.logo" />
                <div styleName="styles.container">
                    {this.state.is_loading ? (
                        <div className="white" styleName="styles.loading">
                            Загрузка...
                        </div>
                    ) : (
                        <form styleName="form.form styles.form">
                            <div styleName="form.heading styles.heading">
                                Вход в Университет
                            </div>
                            <div styleName="styles.formGroup">
                                <label htmlFor="username">Пользователь</label>
                                <input
                                    type="text"
                                    name="username"
                                    id="username"
                                    placeholder="введите имя пользователя"
                                />
                                <label htmlFor="password">Пароль</label>
                                <input
                                    type="password"
                                    name="password"
                                    id="password"
                                    placeholder="введите пароль"
                                />
                            </div>
                            <div styleName="styles.formGroup styles.forgotPassword">
                                <a href="#">Забыли пароль?</a>
                            </div>
                            <div styleName="form.formGroup styles.formGroup">
                                <button
                                    type="submit"
                                    onClick={this.onHandleLogin}
                                >
                                    Войти
                                </button>
                            </div>
                            <div styleName="styles.help">
                                <div styleName="styles.helpText">
                                    Возникли вопросы?
                                    <br />
                                    Обратитесь в службу поддержки
                                </div>
                                <div styleName="styles.helpEmail">
                                    <a href="mailto:email@mail.ru">
                                        email@mail.ru
                                    </a>
                                </div>
                                <div styleName="styles.helpPhone">
                                    8 800 072-71-71
                                </div>
                            </div>
                        </form>
                    )}
                </div>
            </div>
        );
    }
}
// export default LoginPage;
const mapStateToProps = state => {
    return { ...state };
};
export default connect(mapStateToProps)(LoginPage);
