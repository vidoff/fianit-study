import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import key from "weak-key";
import styled from "styled-components";
import styles from "styles/components/Courses.css";

const ImageContainer = styled.div`
    width: 100%;
    height: 100%;
    background-image: url("/images/course_list2.jpg");
    background-image: url('${props => (props.image ? props.image : null)}');
    background-repeat: no-repeat;
    background-position: center center;
    background-size: cover;
    border-radius: 4px;
    min-height: 280px;
    max-width: 360px;
    position: relative;
    overflow: hidden;
`;

class CoursesTableView extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div styleName="styles.grid">
                {this.props.is_loaded
                    ? this.props.items.map(el => {
                          return (
                              <ImageContainer key={key(el)} image={el.image}>
                                  <Link to={`/courses/${el.id}`} styleName="styles.gradient">
                                      <div styleName="styles.wrapper">
                                          <h2 styleName="styles.header">
                                              {el.name}
                                          </h2>
                                          <div styleName="styles.status">
                                              {el.status_name}
                                          </div>
                                      </div>
                                  </Link>
                              </ImageContainer>
                          );
                      })
                    : null}
            </div>
        );
    }
}

CoursesTableView.propTypes = {
    items: PropTypes.array.isRequired,
    is_loaded: PropTypes.bool.isRequired
};

export default CoursesTableView;
