import React, { Component } from "react";
import key from "weak-key";
import Checkbox from "components/Forms/Checkbox";
import filter from "styles/common/filter.css";
import ButtonToggleView from "components/Forms/Buttons/ButtonToggleView";
import SelectBox from "components/Forms/Select/SelectBox";
import listImage from "static/images/viewList.svg";
import tableImage from "static/images/viewTable.svg";

class CoursesFilter extends Component {
    constructor(props) {
        super(props);
    }
    onHandleCheckboxChange = event => {
        this.setState({ checked: event.target.checked });
    };

    render() {
        const handlers = { ...this.props.handlers };
        let data = this.props.data;
        return (
            <div styleName="filter.filter">
                <div styleName="filter.filterGroup filter.checkboxes">
                    <label>
                        <Checkbox
                            checked={data.myCourses}
                            onChange={handlers.handleChangeMyCourses}
                        />
                        <span>Мои курсы</span>
                    </label>
                    <label>
                        <Checkbox
                            checked={data.avCourses}
                            onChange={handlers.handleChangeAvCourses}
                        />
                        <span>Доступные курсы</span>
                    </label>
                </div>
                <div styleName="filter.toggleView">
                    <ButtonToggleView
                        onClick={handlers.handleToggleView}
                        dataView={"table"}
                        image={tableImage}
                    />
                    <ButtonToggleView
                        onClick={handlers.handleToggleView}
                        dataView={"list"}
                        image={listImage}
                    />
                </div>
                <div styleName="filter.selectBox">
                    <SelectBox
                        caption="Фильтрация по статусу"
                        className={filter.selectMultiple}
                    >
                        {data.statuses.map(el => (
                            <label key={key(el)}>
                                <Checkbox
                                    value={el.id}
                                    onChange={handlers.handleChangeStatus}
                                    checked={el.checked}
                                    fill="#1561d4"
                                />
                                <span>{el.name}</span>
                            </label>
                        ))}
                    </SelectBox>
                </div>
            </div>
        );
    }
}
export default CoursesFilter;
