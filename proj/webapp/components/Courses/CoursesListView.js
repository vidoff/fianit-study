import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import key from "weak-key";
import styled from "styled-components";
import styles from "styles/components/Courses.css";
import ButtonDefault from "components/Forms/Buttons/ButtonDefault";

const ImageContainer = styled.div`
    width: 100%;
    height: 100%;
    background-image: url("/images/course_list2.jpg");
    background-image: url('${props => (props.image ? props.image : null)}');
    background-repeat: no-repeat;
    background-position: center center;
    background-size: cover;
    border-radius: 4px;
    min-height: 280px;
    max-width: 360px;
    position: relative;
    overflow: hidden;
`;
const options = {};

class CoursesListView extends Component {
    constructor(props) {
        super(props);
        this.state = Object.assign({}, options, { dataIsLoaded: false });
    }

    render() {
        return (
            <div styleName="styles.list">
                {this.props.is_loaded
                    ? this.props.items.map(el => {
                          return (
                              <div styleName="styles.row" key={key(el)}>
                                  <div styleName="styles.rowName">
                                      {el.name}
                                  </div>
                                  <div styleName="styles.rowStatus">
                                      {el.status_name}
                                  </div>
                                  <div styleName="styles.rowLink">
                                      <ButtonDefault to={`/courses/${el.id}`}>узнать поробнее</ButtonDefault>
                                  </div>
                              </div>
                          );
                      })
                    : "Загрузка"}
            </div>
        );
    }
}

CoursesListView.propTypes = {
    items: PropTypes.array.isRequired,
    is_loaded: PropTypes.bool.isRequired
};

export default CoursesListView;
