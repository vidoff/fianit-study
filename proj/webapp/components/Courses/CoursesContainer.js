import React, { Component } from "react";
import { connect } from "react-redux";
import { getCoursesAction } from "../../actions/getCourses";
import PropTypes from "prop-types";
import styles from "styles/components/Courses.css";
import filter from "styles/common/filter.css";
import Header from "components/Header";
import CoursesFilter from "./CoursesFilter";
import CoursesTableView from "./CoursesTableView";
import CoursesListView from "./CoursesListView";

const options = {
    statuses: [
        { id: 3, checked: false, name: "Завершен" },
        { id: 2, checked: false, name: "В процессе" }
    ],
    view: "table"
};

class Courses extends Component {
    constructor(props) {
        super(props);
        this.opts = {
            title: "Мои курсы"
        };
        this.state = Object.assign({}, options, {
            myCourses: false,
            avCourses: false,
            filterStatus: false
        });
    }
    filterByStatus(status_id) {
        let statusItem = {};
        for (let i = 0; i < this.state.statuses.length; i++) {
            statusItem = this.state.statuses[i];
            if (statusItem.id == Number(status_id) && statusItem.checked)
                return true;
        }
        return false;
    }

    handleChangeMyCourses = event => {
        this.setState({ myCourses: event.target.checked });
    };
    handleChangeAvCourses = event => {
        this.setState({ avCourses: event.target.checked });
    };
    handleChangeStatus = event => {
        const value = event.target.value;
        this.setState(state => {
            let filtred = false;
            return {
                statuses: state.statuses.map(el => {
                    if (el.id == value) el.checked = !el.checked;
                    if (el.checked) filtred = true;
                    return el;
                }),
                filterStatus: filtred
            };
        });
    };
    handleToggleView = event => {
        this.setState({ view: event.target.dataset.view });
    };
    updateByHash(hash) {
        hash = hash.substring(1);
        if (hash !== this.state.view) {
            this.setState({ view: hash });
        }
    }
    componentDidMount() {
        const data = {};
        if (this.props.location.hash) {
            this.updateByHash(this.props.location.hash);
        }
        if (!this.props.courses.is_loaded) {
            this.props.dispatch(getCoursesAction(data));
        }
    }

    updateView(hash) {
        this.setState({ view: hash });
    }

    getItems() {
        let items = [];

        if (this.props.courses.is_loaded) {
            if (!this.state.filterStatus) {
                return this.props.courses.items;
            } else {
                items = this.props.courses.items.filter(el =>
                    this.filterByStatus(el.status)
                );
            }
        }
        return items;
    }

    render() {
        const handlers = {
            handleChangeMyCourses: this.handleChangeMyCourses,
            handleChangeAvCourses: this.handleChangeAvCourses,
            handleChangeStatus: this.handleChangeStatus,
            handleToggleView: this.handleToggleView
        };
        return (
            <div className="layout stretch">
                <Header title={this.opts.title}>
                    <CoursesFilter
                        data={this.state}
                        handlers={handlers}
                        statuses={this.state.statuses}
                    />
                </Header>
                <section className="wrapper">
                    <main className="content">
                        {this.state.view == "list" ? (
                            <CoursesListView
                                is_loaded={this.props.courses.is_loaded}
                                items={this.getItems()}
                            />
                        ) : (
                            <CoursesTableView
                                is_loaded={this.props.courses.is_loaded}
                                items={this.getItems()}
                            />
                        )}
                    </main>
                </section>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        courses: state.courses
    };
};

Courses.propTypes = {
    courses: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    location: PropTypes.object.isRequired
};
export default connect(mapStateToProps)(Courses);
