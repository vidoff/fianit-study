import React, { Component } from "react";
import DataProvider from "./DataProvider";
import Table from "./Table";
// import Wrapper from "./test/Wrapper";
// import Left from "./test/Left";
// import Right from "./test/Right";
import SideBar from "./SideBar";
// import { Route, Link, BrowserRouter as Router } from "react-router-dom";

class Main extends Component {
    render() {
        return (
            <div>
                <SideBar />
                <DataProvider
                    endpoint="api/lead/"
                    render={(data, rotating) => (
                        <div>
                            <Table data={data} rotating={rotating} />
                        </div>
                    )}
                />
            </div>
        );
    }
}
export default Main;
