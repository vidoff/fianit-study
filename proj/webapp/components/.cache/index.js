import React from "react";
import PropTypes from "prop-types";
import key from "weak-key";
import Test from "../test/Test";
// import "../../static/css/App.css";
// import "styles/Table.css";
// import "styles/Table.css";
// import style from "styles/Table.css";
// import "App.css";

const Table = ({ data, rotating }) =>
    !data.courses.length ? (
        <p>Nothing to show</p>
    ) : (
        <div className="column">
            <h1>{data.index}</h1>
            <h2 className="subtitle">
                Showing <strong>{data.courses.length} items</strong>
            </h2>
            <Test rotating={rotating} />
            <table className="table is-striped">
                <thead>
                    <tr>
                        {Object.entries(data.courses[0]).map(el => (
                            <th key={key(el)}>{el[0]}</th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {data.courses.map(el => (
                        <tr key={el.id}>
                            {Object.entries(el).map(el => (
                                <td key={key(el)}>{el[1]}</td>
                            ))}
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
Table.propTypes = {
    data: PropTypes.object.isRequired,
    rotating: PropTypes.bool.isRequired
};
Table.componentDidMount = function() {
    console.log("table mount");
};

export default Table;
