import React, { Component } from "react";

class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            input: "test value"
        };
        this.handleOnChange = this.handleOnChange.bind(this);
        this.handleOnClick = this.handleOnClick.bind(this);
    }
    handleOnChange(event) {
        this.setState({
            input: event.target.value
        });
    }

    handleOnClick(event) {
        console.log(this.state.input);
        event.preventDefault();
    }

    render() {
        return (
            <div>
                <input
                    type="text"
                    name="name"
                    value={this.state.input}
                    onChange={this.handleOnChange}
                />
                <textarea
                    name="textarea"
                    cols="30"
                    rows="10"
                    onChange={this.handleOnChange}
                    value={this.state.input}
                />
                <button onClick={this.handleOnClick} >{this.state.input}</button>
            </div>
        );
    }
}

export default Form;
