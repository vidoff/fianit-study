import React, { Component } from "react";
// import key from "weak-key";
import Form from "./Form";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { startAction } from "../../actions/startAction";
import { stopAction } from "../../actions/stopAction";

class Test extends Component {
    constructor(props) {
        super(props);
        this.state = {
            testString: new Date(),
            warn: false,
            rotating: this.props.rotating
        };
        this.handleOnClick = this.handleOnClick.bind(this);
    }
    // static propTypes = {
    //     // testString: PropTypes.string.isRequired
    //     rotating: PropTypes.bool.isRequired
    // };
    componentDidMount() {
        // this.timerID = setInterval(() => this.tick(), 1000);
    }
    componentWillUnmount() {
        clearInterval(this.timerID);
    }
    tick() {
        this.setState({
            testString: new Date()
        });
    }
    handleOnClick() {
        this.props.rotating ? this.props.stopAction() : this.props.startAction();

        this.setState(state => ({
            warn: !state.warn,
            rotating: !state.rotating
        }));
    }

    render() {
        const items = ["asdfasdf", 2, 3];
        return (
            <div>
                <h1>
                    <p>{this.state.rotating ? "yes" : "no"}</p>
                    <div>{this.state.warn ? "warn" : "unwarn"}</div>
                    <button onClick={this.handleOnClick}>
                        {this.state.warn ? "yes" : "no"}
                    </button>
                </h1>
                <ul>
                    {items.map((el, i) => (
                        <li key={i}>{el}</li>
                    ))}
                </ul>
                <Form />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state
});
const mapDispatchToProps = dispatch => ({
    startAction: () => dispatch(startAction),
    stopAction: () => dispatch(stopAction)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Test);
