import React, { Component } from "react";
import PropTypes from "prop-types";
import styles from "styles/components/Header.css";

class Header extends Component {
    constructor(props) {
        super(props);
        this.title = this.props.title ? this.props.title : "Заголовок";
    }
    render() {
        let styleName = "styles.header";
        if(this.props.opts && this.props.opts.style=="white")
            styleName = "styles.headerWhite";
        return (
            <header styleName={styleName}>
                <div styleName="styles.container">
                    <div styleName="styles.title">
                        <h1>{this.title}</h1>
                    </div>
                    <div styleName="styles.filter">
                        {this.props.children ? this.props.children : null}
                    </div>
                </div>
            </header>
        );
    }
}

Header.propTypes = {
    opts: PropTypes.object,
    children: PropTypes.object
};
export default Header;
