import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import key from "weak-key";
import TableGrid, {
    TableHeader,
    TableContent,
    Row
} from "components/Forms/Table/TableGrid";
import styles from "styles/components/LectureList.css";
import ButtonDefault from "components/Forms/Buttons/ButtonDefault"

class LectureList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="container">
                <section className="text containerSection">
                    <h2 className="title">Материалы курса:</h2>
                    <TableGrid col="5" fontWeight="semi-bold">
                        <TableHeader>
                            <div>Материал</div>
                            <div>Лекция</div>
                            <div>Тест</div>
                        </TableHeader>
                        <TableContent>
                            <Row>
                                <div>
                                    1. Монеты иностранных государств из
                                    драгоценных металлов
                                </div>
                                <div>Пройдено %</div>
                                <div>96/100</div>
                                <div><a className="linkBlue" href="#">Пройти тестирование</a></div>
                                <ButtonDefault>начать изучение</ButtonDefault>
                            </Row>
                            <Row>
                                <div>
                                    1. Монеты иностранных государств из
                                    драгоценных металлов
                                </div>
                                <div>Пройдено %</div>
                                <div>96/100</div>
                                <div><a className="linkBlue" href="#">Пройти тестирование</a></div>
                                <ButtonDefault>начать изучение</ButtonDefault>
                            </Row>

                        </TableContent>
                    </TableGrid>
                </section>
                <footer className="containerFooter">
                    <div styleName="styles.footer">
                        <div styleName="styles.footerTitle">
                            Итоговый тест по курсу<br/>
                            "Банковские металлы"
                        </div>
                        <div styleName="styles.footerRight">
                            <a href="#">Начать аттестацию</a>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ...state
    };
};

LectureList.propTypes = {
    courses: PropTypes.object.isRequired
};

export default connect(mapStateToProps)(LectureList);
