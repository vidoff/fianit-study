import React, { Component } from "react";
import { Route, Link, BrowserRouter as Router } from "react-router-dom";
import styles from "styles/components/Library.css";

class Library extends Component {
    render() {
        return (
            <div styleName="styles.library">
                Library
            </div>
        );
    }
}
export default Library;
