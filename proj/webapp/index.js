import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import configureStore from "./store/configureStore";
import "normalize.css";
import App from "./components/App.js";

const initialState = {
    login: { is_auth: false },
    courses: { is_loaded: false }
};

const store = configureStore(initialState);

const wrapper = document.getElementById("app");
wrapper
    ? ReactDOM.render(
          <Provider store={store}>
              <App />
          </Provider>,
          wrapper
      )
    : null;
